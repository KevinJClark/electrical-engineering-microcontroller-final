#include "timer.h"
void timer_init()
{
		// Enables clock for 32 bit timer (CT32B0)  Page 34
		LPC_SYSCON->SYSAHBCLKCTRL |= (1<<9);  
		
		// timer mode not counter mode  (Page 367)
		LPC_TMR32B0->CTCR = 0; 
	
		// make the TC trigger an interrupt when MR0 matches with TC
		LPC_TMR32B0->MCR = 0x3;   // page 368
	
		// enable the timer interrupt
		NVIC_EnableIRQ(TIMER_32_0_IRQn);
	
		LPC_TMR32B0->TCR = 2;			   // reset timer
		LPC_TMR32B0->TCR = 1;				 // start timer

}

void timer_start(int hertz)
{
	int clock = 48000000 / hertz;
	// place value in match register MR0
	LPC_TMR32B0->MR0 = clock;  // random value for an example
	
	LPC_TMR32B0->TCR = 2;			   // reset timer
	LPC_TMR32B0->TCR = 1;				 // start timer
}

void timer_stop(void)
{
	LPC_TMR32B0->TCR = 2;			   // reset timer
}
