#include <system_LPC11xx.h>
#include <LPC11xx.h>
#include "LCD_driver.h"
#include "keypad_scanner.h"
#include "timer.h"
#include <stdlib.h> // for atoi()
#include "UART.h"
#include <string.h> // for strcpy()

extern char transmit_string_buffer[]; //Char array
extern char UART_received_buffer[];
extern char received_string[];
extern char UART_received_flag;
extern char UART_received_index; 
extern char UART_received_byte;
extern char saved_string[];
extern int timer_int_flag;
extern int first_keypress;

void adc_init(void)
{
		// set pin 4 (P0.11) as ADC 
	// see page 94 
	LPC_IOCON -> R_PIO0_11 = 0x42; //Set to ADC0 and resistor off
	
	// turn off all resistors on pin 4
	// again on page 94
	//LPC_IOCON -> R_PIO0_11 &= ~(0x18); //2_0 0XXX Set to no resistor

	
	// Set the pin as analog mode (not digital) 
	// Once again, page 94 
	//LPC_IOCON -> R_PIO0_11 &= ~(0x8);
	
	
	 // Enable the clock and power for ADC 
	 // Refer to pages 34 and 45
	LPC_SYSCON -> SYSAHBCLKCTRL |= 0x2000; //Bit 13 on = ADC clock on
	
	LPC_SYSCON -> PDRUNCFG &= ~(0x10); //Bit 4 set 0 = ADC powered
	
// the last two lines of code are supplied. 	
	
	// Enables AD[0], sets burst mode to 1 (hardware not software scan)
	// Also sets the clock speed to just under 4.5MHz
	LPC_ADC->CR |= (0x10A01);    // check pages 410 -- 411
	
	// According to datasheet, bit 8 of the register below must be set to 0 if in burst mode
	LPC_ADC->INTEN &= ~(1<<8);  // check page 413
}

void temp_control(int* sample_control) //I love passing arrays in by reference *puke*
{
	//int sample_control[3]; passed in by reference
	
	//Variable init -- Keypad
	int keyvalue;
	int key = '^';
	
	while(1) //Outer while -- Repeat menu if # button is pressed
	{
		sample_control[0] = 0; //Sample Frequency
		sample_control[1] = 0; //Sample Number
		sample_control[2] = 0; //Return-to-menu boolean ... ('*')
		LCD_clear();
		LCD_print_string("Sample Freq:");
		LCD_command(0xC0,0); //Newline
		UART_send_string("Sample Freq:\r\n");
	
		while(1) //First inner while -- Take keypad input for screen 1: Sample Frequency
		{
			key = scanner();
			if(key != '^') //Real key was pressed
			{
				while(key == scanner()); //Wait until user lifts their finger off the key to continue
				if(key == '*')
				{
					sample_control[2] = 1; //Set return flag
					return;
				}
				else if(key == 'S') //UART string available...
				{
					sample_control[0] = atoi(saved_string);
					sprintf(transmit_string_buffer, "%d", sample_control[0]);
					UART_send_string(transmit_string_buffer);
					UART_send_string("\r\n");
					LCD_print_string(transmit_string_buffer);
					
					for(int i=0; i<1000000; i++); //Debounce
					
					LCD_clear();
					LCD_print_string("D: Start  Sample");
					LCD_command(0xC0,0); //Newline
					LCD_print_string("Num: ");
					UART_send_string("D: Start  Sample\r\n");
					UART_send_string("Num:\r\n");
					
					break;
				}
				else if(key == '#') //Next screen
				{
					LCD_clear();
					LCD_print_string("D: Start  Sample");
					LCD_command(0xC0,0); //Newline
					LCD_print_string("Num: ");
					UART_send_string("D: Start  Sample\r\n");
					UART_send_string("Num:\r\n");
					break; //Go to next while loop
				}
				else if(key >= '0' && key <= '9') //Numeric
				{
					keyvalue = key - '0';
					sample_control[0] = sample_control[0] * 10 + keyvalue;
					LCD_print_char(key);
				}
			}
		}
			
			//Same thing but for the next value
		while(1) //Second inner while -- Take keypad input for screen 2: Sample number
		{
			key = scanner();
			if(key != '^') //Real key was pressed
			{
				while(key == scanner()); //Wait until user lifts their finger off the key to continue
				if(key == '*')
				{
					sample_control[2] = 1; //Set return flag
					return;
				}
				else if(key == 'S')
				{
					sample_control[1] = atoi(saved_string);
					sprintf(transmit_string_buffer, "%d", sample_control[1]);
					UART_send_string(transmit_string_buffer);
					UART_send_string("\r\n");
					LCD_print_string(transmit_string_buffer);
					return; //Data finished being collected
				}
				else if(key == '#')
				{
					break; //Reset to beginning of menu and redo all temp data entry
				}
				else if(key == 'D' || key == 'd') //Start TEMP measurement
				{
					return; //Return out of the function and give the sample_control data
				}
				else if(key >= '0' && key <= '9') //Numeric
				{
					keyvalue = key - '0';
					sample_control[1] = sample_control[1] * 10 + keyvalue;
					LCD_print_char(key);
				}
			}
		}
	}
}


void temp()
{
	//Variable init -- Keypad
	int key;
	
	//Variable init -- Temperature
	int ADC_value = 0;
	int F = 0;
	int C = 0;
	int F_sum = 0;
	int C_sum = 0;
	int C_average = 0;
	int F_average = 0;
	char degree_symbol = 0xDF;  // 0xDF is the degree symbol on Hitatchi HD44780 driver
	int sample_control[3]; //Where [0] is the sample frequency, [1] is the sample number, and [2] is a reset bool
	
	temp_control(sample_control); //Arrays are automatically passed by reference
	timer_start(sample_control[0]);
	if(sample_control[2])
	{
		return; //Back to main menu for function select
	}
	
	//Print some stuff on the screen so the user knows the temp is coming... Just needs a bit of time to get those numbers
	LCD_clear();
	LCD_print_string("Reading");
	LCD_command(0xC0,0); //Newline
	LCD_print_string("Thermistor");
	UART_send_string("Reading Thermistor\r\n");

			
	while(1)
	{
		for(int i=0; i<sample_control[1]; i++)
		{
			while(timer_int_flag == 0)
			{
				//Key processing code ... Check for '#' or '*'
				key = scanner();
				if(key == '*') //Back to main()
				{
					while(key == scanner()); //Wait until user lifts their finger off the key to continue
					timer_stop();
					return;
				}
				else if(key == '#') //Reset freq and sample values
				{
					while(key == scanner()); //Wait until user lifts their finger off the key to continue
					temp_control(sample_control);
					timer_start(sample_control[0]);
					F_sum = 0;
					C_sum = 0;
					LCD_clear();
					LCD_print_string("Reading");
					LCD_command(0xC0,0); //Newline
					LCD_print_string("Thermistor");
					UART_send_string("Reading Thermistor\r\n");
				}
			}
			timer_int_flag = 0; //Consume timer interrupt here
			ADC_value = (LPC_ADC->DR[0] >> 6) & (0x3FF);     
			C = 3.33 / (1023 / ADC_value) * 100; // The temperature is now in celsius
			F = C * 1.8 + 32; // converted into farenheit
			F_sum = F_sum + F;
			C_sum = C_sum + C;
		}
		
		F_average = F_sum / sample_control[1];			// compute average
		C_average = C_sum / sample_control[1];
		
		LCD_clear();
		LCD_print_number(F_average);
		LCD_print_char(degree_symbol);  // print degree symbol
		LCD_print_string("F");				// print F
		LCD_command(0xC0,0); 					//Newline
		LCD_print_number(C_average);
		LCD_print_char(degree_symbol);  // print degree symbol
		LCD_print_string("C");				// print C
		
		sprintf(transmit_string_buffer, "%dF       %dC", F_average, C_average);
		UART_send_string(transmit_string_buffer);
		UART_send_string("\r\n");
		
		F_sum = 0;
		C_sum = 0;
		
		key = scanner();
		if(key != '^') //Real key was pressed
		{
			while(key == scanner()); //Wait until user lifts their finger off the key to continue
			if(key == '*')
			{
				//first_keypress = 1;
				timer_stop();
				return;
			}
			else if(key == '#') //Reset
			{
				temp_control(sample_control);
				timer_start(sample_control[0]);
			}
		}
	}
}

