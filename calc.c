#include <system_LPC11xx.h>
#include <LPC11xx.h>
#include "LCD_driver.h"
#include "keypad_scanner.h"
#include "UART.h"
#include <stdlib.h> // for atoi()

extern char transmit_string_buffer[]; //Char array
extern char UART_received_buffer[];
extern char received_string[];
extern char UART_received_flag;
extern char UART_received_index; 
extern char UART_received_byte;
extern char saved_string[];

void calc()
{
	//Variable init
	int value1 = 0, value2 = 0;
	int keyvalue;
	int key = 'A';
	char op = '#';
	LCD_clear();
	for(int i=0; i<50000 ; i++); //Debounce
	
	while(1)
	{
		key = scanner();
		if(key != '^') //Real key was pressed
		{
			while(key == scanner()); //Wait until user lifts their finger off the key to continue
			if(key == '*')
			{
				LCD_clear();
				return; //Exit out and go back to menu
			}
			
			if(op == 'X') //Equals sign entered, Operation complete.  Reset values for next calculation
			{
				LCD_clear();
				value1 = 0;
				value2 = 0;
				op = '#';
			}
			
			switch(key)
			{
				case 'A' : //Add
				case 'a' :
				case '+' :
					if(op == '#')
					{
						LCD_print_char('+');
						UART_send_string("+\r\n");
						op = '+';
					}
					break;
				case 'B' : //Subtract
				case 'b' :
				case '-' :
					if(op == '#')
					{
						LCD_print_char('-');
						UART_send_string("-\r\n");
						op = '-';
					}
					break;
				case 'C' : //Multiply
				case 'c' :
				case 'x' :
				case 'X' :
					if(op == '#')
					{
						LCD_print_char('*');
						UART_send_string("*\r\n");
						op = '*';
					}
					break;
				case 'D' : //Divide
				case 'd' :
				case '/' :
					if(op == '#')
					{
						LCD_print_char('/');
						UART_send_string("/\r\n");
						op = '/';
					}
					break;
				case 'S' : //String of numbers entered on the UART serial terminal
					
					if(op == '#')
					{
						value1 = atoi(saved_string);
						sprintf(transmit_string_buffer, "%d", value1);
						UART_send_string(transmit_string_buffer);
						UART_send_string("\r\n");
						LCD_print_string(transmit_string_buffer);
					}
					else
					{
						value2 = atoi(saved_string);
						sprintf(transmit_string_buffer, "%d", value2);
						UART_send_string(transmit_string_buffer);
						UART_send_string("\r\n");
						LCD_print_string(transmit_string_buffer);
					}
					
					break;
				case '#' : //Equals
				case '=' :
					LCD_print_char('=');
					switch(op) //Print out appropriate value according to the operator
					{
						case '#' :
							LCD_print_number(value1);
							sprintf(transmit_string_buffer, "%d=%d\r\n", value1, value1);
							break;
						case '+' :
							LCD_print_number(value1 + value2);
							sprintf(transmit_string_buffer, "%d+%d=%d\r\n", value1, value2, (value1 + value2));
							break;
						case '-' :
							LCD_print_number(value1 - value2);
							sprintf(transmit_string_buffer, "%d-%d=%d\r\n", value1, value2, (value1 - value2));
							break;
						case '*' :
							LCD_print_number(value1 * value2);
							sprintf(transmit_string_buffer, "%d*%d=%d\r\n", value1, value2, (value1 * value2));
							break;
						case '/' :
							LCD_print_number(value1 / value2); //Impossible to get floats to work with the LCD code provided
							sprintf(transmit_string_buffer, "%d/%d=%d\r\n", value1, value2, (value1 / value2));
							break;
						
					}
					UART_send_string(transmit_string_buffer);
					op = 'X';
					break;
				default : //Case: Any integer keypress
					LCD_print_char(key); //Print key to LCD
					UART_send_char(key);
					UART_send_string("\r\n");
					keyvalue = key - '0'; //Convert keypress from char to int
					if(op == '#') //If operator hasn't been entered...
					{
						value1 = value1 * 10 + keyvalue; //Add keyvalue to running total of value1
					}
					else //Operator has been entered. We're on the second number
					{
						value2 = value2 * 10 + keyvalue; //Add keyvalue to running total of value2
					}
					break;
			}
		}
	}
}
