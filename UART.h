#include "LPC11xx.h"

void UART_init(void);
void UART_send_char(char);
void UART_send_string(char[]);

void UART_print_char(char);

void UART_reset(void);

void UART_remove_newline(char array[]);
