#include <system_LPC11xx.h>
#include <LPC11xx.h>
#include "LCD_driver.h"
#include "keypad_scanner.h"
#include "timer.h"
#include "UART.h"
#include <stdlib.h> // for atoi()
#include "SPI.h"
#include "potentiometer.h"

extern char transmit_string_buffer[]; //Char array
extern char UART_received_buffer[];
extern char received_string[];
extern char UART_received_flag;
extern char UART_received_index; 
extern char UART_received_byte;
extern char saved_string[];

void potent_control(int* resist_control)
{
	int keyvalue;
	int key = '^';
	
	while(1) //Outer while -- Repeat menu if # button is pressed
	{
		resist_control[0] = 0; //Sample Frequency
		resist_control[1] = 0; //Return-to-menu boolean ... ('*')
		LCD_clear();
		LCD_print_string("Enter Resistance");
		LCD_command(0xC0,0); //Newline
		LCD_print_string("Ohms: ");
		UART_send_string("Enter Resistance\r\n");
		UART_send_string("Ohms:\r\n");
			
		while(1) //1st inner while -- Take keypad input for resistance
		{
			key = scanner();
			if(key != '^') //Real key was pressed
			{
				while(key == scanner()); //Wait until user lifts their finger off the key to continue
				if(key == '*')
				{
					resist_control[1] = 1; //Set return flag
					return;
				}
				else if(key == 'S')
				{
					resist_control[0] = atoi(saved_string);
					sprintf(transmit_string_buffer, "%d", resist_control[0]);
					UART_send_string(transmit_string_buffer);
					UART_send_string("\r\n");
					LCD_print_string(transmit_string_buffer);
					return; //Data finished being collected
				}
				else if(key == '#')
				{
					break; //Reset to beginning of menu and redo all temp data entry
				}
				else if(key == 'D' || key == 'd') //Start TEMP measurement
				{
					return; //Return out of the function and give the sample_control data
				}
				else if(key >= '0' && key <= '9') //Numeric
				{
					keyvalue = key - '0';
					resist_control[0] = resist_control[0] * 10 + keyvalue;
					LCD_print_char(key);
				}
			}
		}
	}
}

void potentiometer()
{
	//Variable init -- Keypad
	int key;
	int resist_control[2]; //Where [0] is the resistance, [1] is a reset bool
	
	potent_control(resist_control); //Arrays are automatically passed by reference
	if(resist_control[1])
	{
		return; //Back to main menu for function select
	}
		
		LCD_clear();
		LCD_print_string("Voltage divison");
		LCD_command(0xC0,0); //Newline
		LCD_print_string("Measure pins");
		UART_send_string("Voltage divison\r\n");
		UART_send_string("Measure pins\r\n");
		set_resistance(resist_control[0]);
	
	while(1)
	{
		//Print some stuff on the screen so the user knows the voltage is being divided
	
		
		key = scanner();
		if(key != '^') //Real key was pressed
		{
			while(key == scanner()); //Wait until user lifts their finger off the key to continue
			if(key == '*')
			{
				//first_keypress = 1;
				timer_stop();
				return;
			}
			else if(key == '#') //Reset
			{
				potent_control(resist_control);
				LCD_clear();
				LCD_print_string("Voltage divison");
				LCD_command(0xC0,0); //Newline
				LCD_print_string("Measure pins");
				UART_send_string("Voltage divison\r\n");
				UART_send_string("Measure pins\r\n");
				set_resistance(resist_control[0]);
			}
		}
	}
}
