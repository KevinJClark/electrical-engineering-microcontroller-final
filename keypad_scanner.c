#include "LPC11xx.h"
#include "UART.h"
#include <string.h> // for strlen
#include "LCD_driver.h"

extern char transmit_string_buffer[]; //Char array
extern char UART_received_buffer[];
extern char received_string[];
extern char UART_received_flag;
extern char UART_received_index; 
extern char UART_received_byte;

int first_keypress = 1;
char saved_string[20];

/*
Columns: 
				 P0.8
				 P0.9
				 P0.5
				 P0.6
				 
Rows:
				 P1.0
				 P1.1
				 P1.2
				 P1.4
*/
#define rows 0x17
#define columns 0x138

// function that will ground all the rows
void groundRows()
{
	LPC_GPIO1->DATA &= ~(rows);
}



/****************************************************************************************
  This function will set the pins as GPIO, INPUTS/OUTPUTS, and will ground the rows
****************************************************************************************/
void keypad_init()
{
	//declare the 8 pins as GPIO
	LPC_IOCON -> PIO0_8 &= ~(0x7); //Default
	LPC_IOCON -> PIO0_5 &= ~(0x7); //Default
	
	LPC_IOCON -> R_PIO1_0 |= 0x1;  //NOT default
	LPC_IOCON -> R_PIO1_1 |= 0x1;  //NOT default
	LPC_IOCON -> R_PIO1_2 |= 0x1;  //NOT default
	
	LPC_IOCON -> PIO1_4 &= ~(0x7); //Default

	LPC_IOCON -> PIO0_4 &= ~(0x7); //Updated for pin sharing
	LPC_IOCON -> PIO0_3 &= ~(0x7); //Updated to reflect pin sharing
	
	// Set the rows as outputs (LPC_GPIO1-> .... )
	//2_0000 0001 0111 == 0x017 == (rows)
	LPC_GPIO1 -> DIR |= (rows);
	
	// Finally, ground the four rows (LPC_GPIO1-> ...)
	groundRows();
}

int getData()
{
	for(int i=0; i<200 ; i++); //Delay
	int data = LPC_GPIO0 -> DATA;
	int P0_345 = data & 0x38;
	int P0_8 = data & 0x100; //Get 8th bit ONLY
	P0_8 = P0_8 >> 2;
	data = P0_8 | P0_345;
	data = data >> 3;
	data = data & 0xF; //ONLY the first 4 bits -- All the rest are 0's
	return data;
}

char scanner()
{
	groundRows();
	char keypress = '^'; //No button pressed -- Default
	int data = getData();
	// perform the scanning proccess here by reading from the columns and 
	//    grounding the various rows
	
	// once the value is found, return it
	if(UART_received_flag) //Bluetooth key pressed
	{
		if(strlen(received_string) <= 2) //Only one character...
		{
			if(first_keypress)
			{
				keypress = received_string[0];
				first_keypress = 0;
			}
			else
			{
				keypress = received_string[1];
			}
			UART_reset();
			return keypress;
		}
		else
		{
			strcpy(saved_string, received_string);
			UART_reset();
			return 'S'; //String signal
		}
	}

	switch(data)
	{
		case 15 :
			break;
		
		case 13 :
			groundRows();
			LPC_GPIO1->DATA |= 0x07;
			if(getData() == 13)
			{
				keypress = '0';
			}
			groundRows();
			LPC_GPIO1->DATA |= 0x13;
			if(getData() == 13)
			{
				keypress = '8';
			}
			groundRows();
			LPC_GPIO1->DATA |= 0x15;
			if(getData() == 13)
			{
				keypress = '5';
			}
			groundRows();
			LPC_GPIO1->DATA |= 0x16;
			if(getData() == 13)
			{
				keypress = '2';
			}
			break;
		
		case 14 :
			groundRows();
			LPC_GPIO1->DATA |= 0x07;
			if(getData() == 14)
			{
				keypress = 'D';
			}
			groundRows();
			LPC_GPIO1->DATA |= 0x13;
			if(getData() == 14)
			{
				keypress = 'C';
			}
			groundRows();
			LPC_GPIO1->DATA |= 0x15;
			if(getData() == 14)
			{
				keypress = 'B';
			}
			groundRows();
			LPC_GPIO1->DATA |= 0x16;
			if(getData() == 14)
			{
				keypress = 'A';
			}
			break;
		
		case 11 :
			groundRows();
			LPC_GPIO1->DATA |= 0x07;
			if(getData() == 11)
			{
				keypress = '#';
			}
			groundRows();
			LPC_GPIO1->DATA |= 0x13;
			if(getData() == 11)
			{
				keypress = '9';
			}
			groundRows();
			LPC_GPIO1->DATA |= 0x15;
			if(getData() == 11)
			{
				keypress = '6';
			}
			groundRows();
			LPC_GPIO1->DATA |= 0x16;
			if(getData() == 11)
			{
				keypress = '3';
			}
			break;
		
		case 7 :
			groundRows();
			LPC_GPIO1->DATA |= 0x07;
			if(getData() == 7)
			{
				keypress = '*';
			}
			groundRows();
			LPC_GPIO1->DATA |= 0x13;
			if(getData() == 7)
			{
				keypress = '7';
			}
			groundRows();
			LPC_GPIO1->DATA |= 0x15;
			if(getData() == 7)
			{
				keypress = '4';
			}
			groundRows();
			LPC_GPIO1->DATA |= 0x16;
			if(getData() == 7)
			{
				keypress = '1';
			}
			break;
	}
	
	groundRows();
	return keypress;
}
