#include <system_LPC11xx.h>
#include <LPC11xx.h>
#include <stdio.h> // for sprintf()
#include <string.h> // for memcpy()
#include "LCD_driver.h"
#include "keypad_scanner.h"
#include "menu.h"
#include "calc.h"
#include "temp.h"
#include "UART.h"
#include "timer.h"
#include "potentiometer.h"
#include "SPI.h"


// global variables -- Externally sourced from UART.c
extern char transmit_string_buffer[]; //Char array
extern char UART_received_buffer[];
extern char received_string[];
extern char UART_received_flag;
extern char UART_received_index; 
extern char UART_received_byte;
extern int first_keypress;
extern char saved_string[];

int timer_int_flag = 0;

void TIMER32_0_IRQHandler(void)
{
	// clear the interrupt flag
	LPC_TMR32B0->IR |= 1;  // do this first
	
	
	// add all other code you would like here
	
	
	// if you have modified the value in the match register (MR0), then you should reset the timer
	// If you have not, then the option to reset the timer is up to you 
	LPC_TMR32B0->TCR = 2;			   // reset timer
	LPC_TMR32B0->TCR = 1;				 // start timer
	
	timer_int_flag = 1;
}

void UART_IRQHandler(void)
{
	UART_received_byte = LPC_UART -> RBR; // read in the byte
	
	if(UART_received_byte == 0x0D) // if "Enter" key was read in, set the flag since all data is received
	{
		UART_received_flag = 1; 
		
		// now use memcpy() function to convert the char array into a "string" (included in string.h)
		// this is usefull for comparing the string as shown in main()
		memcpy(received_string, UART_received_buffer, UART_received_index);
		received_string[UART_received_index] = '\0'; // place null character at end of string 
	}
	else // place the char into the string
	{
		UART_received_buffer[UART_received_index] = UART_received_byte;
		UART_received_index++; // increment the index by 1
	}
}

int main()
{
	//Hardware init
	LCD_init();  
	keypad_init();
	adc_init();
	UART_init();
	timer_init();
	SPI_init();
	
	int key = '^';
	menu();
	
	while(1)
	{
		key = scanner();
		if(key != '^') //Real key pressed
		{
			while(key == scanner()); //Wait until user lifts their finger off the key to continue
			UART_print_char(key);
			UART_send_string("\r\n");
			LCD_print_char(key);
			switch(key)
			{
				case 'A' :
				case 'a' :
					calc();
					break;
				case 'B' :
				case 'b' :
					temp();
					break;
				case 'C' :
				case 'c' :
					potentiometer();
					break;				
			}
			
			menu();
		}
	}
}
