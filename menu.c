#include <system_LPC11xx.h>
#include <LPC11xx.h>
#include "LCD_driver.h"
#include "UART.h"

void menu()
{
	LCD_clear();
	LCD_print_string("A: Calc  B: Temp");
	LCD_command(0xC0,0); //Newline
	LCD_print_string("C: DDS   *: Menu");
	
	UART_send_string("A: Calc  B: Temp\r\n");
	UART_send_string("C: DDS   *: Menu\r\n");
}
