# Electrical-Engineering-Microcontroller-Final

C Program written for the LPC1114 to implement a calculator, temperature sensor, and variable voltage divider.  
Support for interleaved Bluetooth and Keypad/LCD I/O.  Code written with the ARM32 processor architecture in mind and originally compiled with armcc.exe

Software development was done in Keil uvision and used an ST-LINK programmer to flash new binaries to the microcontroller.

Hardware Required:
1.  LPC1114FN28 Microcontroller
2.  ST-LINK USB Programmer
3.  Hitachi 1602H LCD
4.  HC-05 (ZS-040) UART Bluetooth Module
5.  MCP41010 Electronic Potentiometer
6.  LM34LP Thermistor
7.  4x4 Button Matrix Keypad
8.  3 Resistors 10K Ohm
9.  Simple Twist Potentiometer
10.  Button
11.  Breadboard
12.  Assorted Wires

Functional flowchart, circuit diagram, and more pictures available below.

**User I/O**  
The first design challenge for this project was user input and output.  The two systems responsible for user I/O are Bluetooth and LCD/keypad.

Code for user I/O can be found in the [keypad_scanner.c](../keypad_scanner.c) file.  The most important keypad function, `scanner()`, is in charge of reading in data either from Bluetooth or from the keypad.  If it gets a Bluetooth interrupt and the string is a single character, it returns returns that character.  Otherwise if it’s a string, the value ‘S’ is returned to indicate the Bluetooth buffer contains a string.  If a key value is entered, the scanning process is used to determine which key was pressed.  If no key is being pressed, `scanner()` returns ‘^’ to indicate no real key was pressed.  This is especially important because a polling method was used in the `scanner()` function, and it is run through thousands of times a second.

Bluetooth code can be found in the [UART.c](../UART.c) file.  Bluetooth is surprisingly simpler to implement than the keypad as the chip itself takes care of anything related to the Bluetooth protocol.  Data simply needs to be sent and received from the Bluetooth module.  To do this, the receive data pin is set up to interrupt upon receiving data from the module.  All Bluetooth data is saved to a global character array and can be used later by client programs.  Data can be sent to the Bluetooth device simply by calling `UART_send_string()` which sends each character out of the transmit pin one at a time.

**Calculator**  
Code for the calculator can be found in the [calc.c](../calc.c) file.  The calculator toggles between input for value 1 and value 2.  Using the previously mentioned `scanner()` function, `calc()` accumulates key values into its operands until an operator is entered or the equals sign is pressed.  When an operator is entered, the accumulation moves on to value 2.  When the equals sign is pressed, value 1 and value 2 are combined with the previously entered operator, and the result is displayed to the LCD and Bluetooth terminal.  Both values and operator are reset so the user can enter their next calculation.

**Thermometer**  
Code for the thermometer can be found in the [temp.c](../temp.c) file.  The first important piece of this file is the `adc_init()` function.  It is in charge of setting pin 4 to be an ADC pin.  Input voltage from the LM34 thermistor is analog and must be converted into a digital value before the LPC1114 can use its temperature data.

Next is the `temp_control()` function.  It takes an array passed by reference as a parameter and modifies the data so that the calling function can use it in its calculations.  Like the previous `calc()` function, `temp_control()` toggles between two modes.  It has an input section for the first screen, which requests the sample frequency and an input section for the second screen, asking for the number of samples.

Data gathered in `temp_control()` is used in `temp()` to initiate the timer with the correct match register value.  Data entered for number of samples is also used to determine how many timer interrupts and temperature samples are required before a temperature average is printed to the LCD/Bluetooth.  This function also checks for the ‘#’ key in case a user wants to enter new frequency and sample parameters.

**Voltage Divider**  
Code for the voltage divider is found in the [potentiometer.c](../potentiometer.c) file.  Like the previous `temp_control()` function, a function called `potent_control()` is used to handle all menus and user input.  The user gives their desired resistance value in Ohms and `potent_control()` passes this value back to the calling function.

The `potentiometer()` function receives an Ohm value from `potent_control()` and does the calculations necessary to push to the electronic potentiometer.  Once the resistance value is entered, pins are set up to read a reduced voltage based on the programmed resistance.  The user can also go back to enter a new resistance value if desired.



**Hardware**  
![Hardware](/uploads/5493d1c4b7bc6e35cbd6dd30da928b77/Hardware.jpg)

**Circuit Diagram**  
![Circuit_Diagram](/uploads/91533baa11e0ee1b4900095919d3cc6b/Circuit_Diagram.png)

**Program Flow Chart**  
![program_flow_chart](/uploads/f00e68a8c32b1cb9d3cebd452babce03/program_flow_chart.png)

**Bluetooth UART**  
![UART](/uploads/4faf7ecea53f83620810e17c5e9dfa20/UART.png)